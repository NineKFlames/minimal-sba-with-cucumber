package cheberiak.artem.sbawithcucumber.functest;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy;

@RunWith(CucumberWithSerenity.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@CucumberOptions(features = "src/test/resources/features", glue = "cheberiak.artem.sbawithcucumber.functest.steps")
public class CucumberRunnerIT {
    @ClassRule
    public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:14.4")
            .withDatabaseName("functest-" + RandomStringUtils.randomAlphanumeric(10))
            .withUsername(RandomStringUtils.randomAlphanumeric(10))
            .withPassword(RandomStringUtils.randomAlphanumeric(30))
            .withExposedPorts(5432)
            .waitingFor(new HostPortWaitStrategy());

    static {
        POSTGRES.start();
        System.setProperty("spring.datasource.url", POSTGRES.getJdbcUrl());
        System.setProperty("spring.datasource.username", POSTGRES.getUsername());
        System.setProperty("spring.datasource.password", POSTGRES.getPassword());
    }
}
