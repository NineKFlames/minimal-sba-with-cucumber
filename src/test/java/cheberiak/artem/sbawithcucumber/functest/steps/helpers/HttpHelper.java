package cheberiak.artem.sbawithcucumber.functest.steps.helpers;

import cheberiak.artem.sbawithcucumber.functest.world.World;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;

@Component
@RequiredArgsConstructor
public class HttpHelper {
    @Getter
    @Setter
    private String queryParam;

    @Getter
    @Setter
    private Entity<?> putEntity;

    @Getter
    @Setter
    private Entity<?> postEntity;

    private final World world;
    private Client client = null;

    public void cleanUp() {
        client = ClientBuilder.newClient();
        client.register(new JacksonJaxbJsonProvider());
        queryParam = null;
        putEntity = null;
        postEntity = null;
    }

    public void get(String endpoint) throws URISyntaxException {
        WebTarget ipTarget = client.target(world.getServerURL().toURI()).path(endpoint);
        Response response = ipTarget.request().get();
        recordResponse(response);
    }

    public void put(String endpoint) throws URISyntaxException {
        WebTarget ipTarget = client.target(world.getServerURL().toURI()).path(endpoint);
        Response response = ipTarget.request().put(putEntity);
        recordResponse(response);
    }

    public void post(String endpoint) throws URISyntaxException {
        WebTarget ipTarget = client.target(world.getServerURL().toURI()).path(endpoint);
        Response response = ipTarget.request().post(postEntity);
        recordResponse(response);
    }

    private void recordResponse(Response response) {
        world.setResponseStatus(response.getStatus());
        world.setResponseString(response.readEntity(String.class));
    }

    public void delete(String endpoint) throws URISyntaxException {
        WebTarget ipTarget = client.target(world.getServerURL().toURI()).path(endpoint);
        Response response = ipTarget.request().delete();
        recordResponse(response);
    }
}
