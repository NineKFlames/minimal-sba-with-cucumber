package cheberiak.artem.sbawithcucumber.functest.steps;

import cheberiak.artem.sbawithcucumber.functest.steps.helpers.HttpHelper;
import cheberiak.artem.sbawithcucumber.functest.testdatamodel.HealthResponse;
import cheberiak.artem.sbawithcucumber.functest.world.World;
import cheberiak.artem.sbawithcucumber.model.dto.ErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HttpSteps {
    private final ObjectMapper mapper = new ObjectMapper();

    @LocalServerPort private int port;

    @Autowired private World world;

    @Autowired private HttpHelper httpHelper;

    @Before
    public void before() {
        world.cleanUp();
        httpHelper.cleanUp();
    }

    @Given("^the backend is located on localhost with random local server port$")
    public void serverIsLocatedAtLocalhostRandomPort() throws MalformedURLException {
        world.setServerURL(new URL("http", "localhost", port, ""));
    }

    @When("^I make a GET request to (.+) endpoint$")
    public void get(String endpoint) throws URISyntaxException {
        httpHelper.get(endpoint);
    }

    @When("^I make a GET request to (.+) endpoint, parametrized with prepared path param$")
    public void getParametrized(String endpointTemplate) throws URISyntaxException {
        httpHelper.get(String.format(endpointTemplate, httpHelper.getQueryParam()));
    }

    @When("^I prepare \"(.+)\" as request path param$")
    public void iPrepareAsRequestPathParam(String param) {
        httpHelper.setQueryParam(param);
    }

    @When("^I make a PUT request to (.+) endpoint with a prepared body, parametrized with prepared path param$")
    public void iMakeAPUTRequestToEndpointWithAPreparedBodyParametrizedWithPreparedPathParam(String endpointTemplate)
            throws URISyntaxException {
        httpHelper.put(String.format(endpointTemplate, httpHelper.getQueryParam()));
    }

    @When("^I make a POST request to (.+) endpoint with a prepared body$")
    public void iMakeAPOSTRequestToEndpointWithAPreparedBody(String endpoint) throws URISyntaxException {
        httpHelper.post(endpoint);
    }

    @When("^I make a DELETE request to (.+) endpoint, parametrized with prepared path param$")
    public void deleteParametrized(String endpointTemplate) throws URISyntaxException {
        httpHelper.delete(String.format(endpointTemplate, httpHelper.getQueryParam()));
    }

    @Then("^I get a (.+) response code$")
    public void getAResponseWithCode(int expectedStatus) {
        assertThat("The response status is incorrect!", world.getResponseStatus(), equalTo(expectedStatus));
    }

    @Then("^response contains a health JSON payload$")
    public void parseResponseStringAsJson() throws IOException {
        // success if parsing hasn't thrown an exception
        world.setHealthResponse(mapper.readValue(world.getResponseString(), HealthResponse.class));
    }

    @Then("^status is \"(.*)\"$")
    public void checkResponseJsonFieldValue(String expectedValue) {
        assertThat("Response status is not the expected one!",
                   world.getHealthResponse().getStatus(),
                   equalTo(expectedValue));
    }

    @Then("^response contains an exception JSON payload$")
    public void parseErrorResponseStringAsJson() throws IOException {
        world.setErrorResponse(mapper.readValue(world.getResponseString(), ErrorResponse.class));
    }

    @Then("^an exception of type (.+) was thrown$")
    public void anExceptionOfTypeWasThrown(String className) {
        assertThat("Unexpected type name!", world.getErrorResponse().getException(), equalTo(className));
    }

    @Then("^the exception contains text \"(.+)\"$")
    public void theExceptionContainsText(String exceptionMessage) {
        assertThat(
                "Exception text doesn't contain expected text!",
                world.getErrorResponse().getMessage(),
                containsString(exceptionMessage));
    }
}