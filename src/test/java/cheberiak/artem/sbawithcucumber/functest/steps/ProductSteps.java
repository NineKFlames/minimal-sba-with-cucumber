package cheberiak.artem.sbawithcucumber.functest.steps;

import cheberiak.artem.sbawithcucumber.functest.steps.helpers.HttpHelper;
import cheberiak.artem.sbawithcucumber.functest.steps.helpers.ProductTableHelper;
import cheberiak.artem.sbawithcucumber.functest.world.Products;
import cheberiak.artem.sbawithcucumber.functest.world.World;
import cheberiak.artem.sbawithcucumber.model.dto.ProductCreationRequest;
import cheberiak.artem.sbawithcucumber.model.dto.ProductListResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductUpdateRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductSteps {
    private final ObjectMapper mapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);

    @Autowired private HttpHelper httpHelper;

    @Autowired private ProductTableHelper productTableHelper;

    @Autowired private Products products;

    @Autowired private World world;

    @Before
    public void before() {
        products.cleanUp();
        productTableHelper.refresh();
    }

    @After
    public void after() {
        var dummyProductIds = products.getDummyProductIds();

        if (dummyProductIds != null) {
            dummyProductIds.add(products.getDummyProductId());
        }

        if (dummyProductIds != null) {
            dummyProductIds.removeIf(Objects::isNull);
            dummyProductIds.forEach(productTableHelper::deleteById);
        }

        productTableHelper.cleanUp();
    }

    @Given("^a product exists with name \"(.+)\"$")
    public void aProductExistsWithName(String name) {
        products.setDummyProductId(productTableHelper.createDummyProductWithName(name).getId());
    }

    @Given("^products exist for names:$")
    public void productsExistForNames(List<String> names) {
        names.forEach(name ->
            products.getDummyProductIds().add(productTableHelper.createDummyProductWithName(name).getId())
        );
    }

    @When("^I know said product's ID upfront and prepare it as request path param$")
    public void prepareProductId() {
        httpHelper.setQueryParam(products.getDummyProductId().toString());
    }

    @When("^I prepare an product update request$")
    public void iPrepareAnProductUpdateRequest() {
        products.setProductUpdateRequest(new ProductUpdateRequest());
    }

    @When("^I put \"(.+)\" name to the update request$")
    public void iPutNameToTheUpdateRequest(String name) {
        products.getProductUpdateRequest().setName(name);
    }

    @When("^I put \"(.+)\" price to the update request$")
    public void iPutPriceToTheUpdateRequest(Double price) {
        products.getProductUpdateRequest().setPrice(BigDecimal.valueOf(price));
    }

    @When("^I prepare a PUT body with prepared product update request$")
    public void iPrepareAPUTBodyWithPreparedProductUpdateRequest() {
        httpHelper.setPutEntity(Entity.entity(products.getProductUpdateRequest(), MediaType.APPLICATION_JSON_TYPE));
    }

    @When("^I prepare an product creation request$")
    public void iPrepareAnProductCreationRequest() {
        products.setProductCreationRequest(new ProductCreationRequest());
    }

    @When("^I put \"(.+)\" name to the creation request$")
    public void iPutNameToTheCreationRequest(String name) {
        products.getProductCreationRequest().setName(name);
    }

    @When("^I put \"(.+)\" price to the creation request$")
    public void iPutPriceToTheCreationRequest(Double price) {
        products.getProductCreationRequest().setPrice(BigDecimal.valueOf(price));
    }

    @When("^I prepare a POST body with prepared product update request$")
    public void iPrepareAPOSTBodyWithPreparedProductUpdateRequest() {
        httpHelper.setPostEntity(Entity.entity(products.getProductCreationRequest(), MediaType.APPLICATION_JSON_TYPE));
    }

    @Then("^response contains a product description JSON payload$")
    public void parseResponseStringAsJson() throws IOException {
        // success if parsing hasn't thrown an exception
        products.setProductResponse(mapper.readValue(world.getResponseString(), ProductResponse.class));
    }

    @Then("^product from response has \"(.+)\" name$")
    public void productFromResponseHasName(String name) {
        assertThat("The name is incorrect!", products.getProductResponse().getName(), equalTo(name));
    }

    @Then("^product from DB has \"(.+)\" name$")
    public void productFromDbHasName(String name) {
        assertThat("The name is incorrect!", products.getProductFromDb().getName(), equalTo(name));
    }

    @Then("^product from DB has \"(.+)\" price$")
    public void productFromDbHasPrice(Double price) {
        assertThat("The price is incorrect!",
                   products.getProductFromDb().getPrice(),
                   comparesEqualTo(BigDecimal.valueOf(price)));
    }

    @Then("^response contains a product descriptions list JSON payload$")
    public void responseContainsAProductDescriptionsListJSONPayload() throws IOException {
        products.setProductListResponse(mapper.readValue(world.getResponseString(), ProductListResponse.class));
    }

    @Then("^the product descriptions list contains (\\d+) elements$")
    public void theProductDescriptionsListContainsElements(int size) {
        assertThat("List of products has incorrect size!", products.getProductListResponse().getList(), hasSize(size));
    }

    @Then("^said product still exists in DB with 'deleted' flag set to '(true|false)'$")
    public void saidProductStillExistsInDBWithDeletedFlagSetTo(boolean expectedDeletedStatus) {
        assertThat("'Deleted' status is incorrect!",
                   productTableHelper.getDeletedFieldForProductWithId(products.getDummyProductId()),
                   equalTo(expectedDeletedStatus));
    }

    @Then("^a product with id from response exists in DB$")
    public void productFromResponseExistsInDB() {
        UUID id = products.getProductResponse().getId();
        products.setProductFromDb(productTableHelper.findById(id));
        products.getDummyProductIds().add(id);
    }

    @Then("^a product with id from path param exists in DB$")
    public void productFromPathParamExistsInDB() {
        products.setProductFromDb(productTableHelper.findById(UUID.fromString(httpHelper.getQueryParam())));
    }
}