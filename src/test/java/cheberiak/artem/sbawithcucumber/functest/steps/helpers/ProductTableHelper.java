package cheberiak.artem.sbawithcucumber.functest.steps.helpers;

import cheberiak.artem.sbawithcucumber.model.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.function.Function;

@Component
public class ProductTableHelper {
    private final EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public ProductTableHelper(@Autowired EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public void refresh() {
        if (entityManager != null && entityManager.isOpen()) {
            if (entityManager.isJoinedToTransaction()) {
                entityManager.flush();
            }

            entityManager.close();
        }

        entityManager = entityManagerFactory.createEntityManager();
    }

    public Product createDummyProductWithName(String name) {
        return withinTransaction(__ -> {
            Product newDummy = new Product(null, name, BigDecimal.valueOf(300), ZonedDateTime.now(), false);
            entityManager.persist(newDummy);
            return newDummy;
        });
    }

    public void deleteById(UUID uuid) {
        withinTransaction(__ -> {
            entityManager.remove(findByIdInternal(uuid));
            return null;
        });
    }

    public Product findById(UUID uuid) {
        return withinTransaction(__ -> findByIdInternal(uuid));
    }

    public Product findByIdInternal(UUID uuid) {
        var product = entityManager.find(Product.class, uuid);
        entityManager.refresh(product);
        return product;
    }

    public boolean getDeletedFieldForProductWithId(UUID uuid) {
        Product byId = findById(uuid);
        return byId.getDeleted();
    }

    public void cleanUp() {
        this.entityManager.close();
    }

    private <T> T withinTransaction(Function<EntityTransaction, T> f) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        final T returnValue = f.apply(transaction);
        transaction.commit();
        return returnValue;
    }
}
