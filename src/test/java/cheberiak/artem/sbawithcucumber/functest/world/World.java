package cheberiak.artem.sbawithcucumber.functest.world;

import cheberiak.artem.sbawithcucumber.functest.testdatamodel.HealthResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ErrorResponse;
import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.net.URL;

@Component
@Data
@ToString
public class World {
    private URL serverURL;
    private String responseString;
    private int responseStatus;
    private HealthResponse healthResponse;
    private ErrorResponse errorResponse;

    public void cleanUp() {
        serverURL = null;
        responseString = null;
        responseStatus = 0;
        healthResponse = null;
        errorResponse = null;
    }
}
