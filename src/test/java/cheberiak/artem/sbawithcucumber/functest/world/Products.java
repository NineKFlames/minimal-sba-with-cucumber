package cheberiak.artem.sbawithcucumber.functest.world;

import cheberiak.artem.sbawithcucumber.model.dto.ProductCreationRequest;
import cheberiak.artem.sbawithcucumber.model.dto.ProductListResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductUpdateRequest;
import cheberiak.artem.sbawithcucumber.model.entity.Product;
import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Component
@Data
@ToString
public class Products {
    private UUID dummyProductId;
    private Set<UUID> dummyProductIds = new HashSet<>();
    private Product productFromDb;
    private ProductResponse productResponse;
    private ProductListResponse productListResponse;
    private ProductUpdateRequest productUpdateRequest;
    private ProductCreationRequest productCreationRequest;

    public void cleanUp() {
        dummyProductId = null;
        productFromDb = null;
        productResponse = null;
        productListResponse = null;
        productUpdateRequest = null;
        productCreationRequest = null;
        dummyProductIds.clear();
    }
}
