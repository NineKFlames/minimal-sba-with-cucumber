package cheberiak.artem.sbawithcucumber.model.dto.converter;

import cheberiak.artem.sbawithcucumber.model.dto.ProductCreationRequest;
import cheberiak.artem.sbawithcucumber.model.entity.Product;

import java.time.ZonedDateTime;

public class ProductRequestToEntityConverter {
    public static Product convert(ProductCreationRequest creationRequest) {
        return new Product(null, creationRequest.getName(), creationRequest.getPrice(), ZonedDateTime.now(), false);
    }
}
