package cheberiak.artem.sbawithcucumber.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@JsonSerialize
@NoArgsConstructor
@AllArgsConstructor
public class ProductCreationRequest {
    @NotBlank(message = "Name must be present!")
    private String name;

    @NotNull(message = "Price must not be null!")
    @Positive(message = "Price must be above zero!")
    private BigDecimal price;
}


