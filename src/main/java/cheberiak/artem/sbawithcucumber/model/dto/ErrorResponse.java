package cheberiak.artem.sbawithcucumber.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonSerialize
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    private Integer httpCode;
    private String message;
    private String exception;
}
