package cheberiak.artem.sbawithcucumber.model.dto.converter;

import cheberiak.artem.sbawithcucumber.model.dto.ProductResponse;
import cheberiak.artem.sbawithcucumber.model.entity.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductToResponseConverter {
    public static ProductResponse convert(Product original) {
        return new ProductResponse(original.getId(), original.getName(), original.getPrice(), original.getCreatedAt());
    }
}
