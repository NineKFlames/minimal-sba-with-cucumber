package cheberiak.artem.sbawithcucumber.service;

import cheberiak.artem.sbawithcucumber.model.dto.ProductCreationRequest;
import cheberiak.artem.sbawithcucumber.model.dto.ProductListResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductUpdateRequest;
import cheberiak.artem.sbawithcucumber.model.dto.converter.ProductRequestToEntityConverter;
import cheberiak.artem.sbawithcucumber.model.dto.converter.ProductToResponseConverter;
import cheberiak.artem.sbawithcucumber.model.entity.Product;
import cheberiak.artem.sbawithcucumber.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public ProductResponse findById(UUID uuid) {
        return ProductToResponseConverter.convert(find(uuid));
    }

    public ProductResponse updateById(UUID uuid, ProductUpdateRequest updateRequest) {
        Product original = find(uuid);
        String newName = updateRequest.getName();
        original.setName(newName == null || newName.isEmpty() ? original.getName() : newName);
        BigDecimal newPrice = updateRequest.getPrice();
        original.setPrice(newPrice == null || newPrice.signum() == 0 || newPrice.signum() == -1 ?
                                  original.getPrice() :
                                  newPrice);
        return save(original);
    }

    private Product find(UUID uuid) {
        return productRepository.findById(uuid)
                                .orElseThrow(
                                        () -> new ProductNotFoundException("Product with ID " + uuid + " not found!")
                                );
    }

    private ProductResponse save(Product product) {
        return ProductToResponseConverter.convert(productRepository.saveAndFlush(product));
    }

    public ProductResponse saveNew(ProductCreationRequest creationRequest) {
        return save(ProductRequestToEntityConverter.convert(creationRequest));
    }

    public ProductListResponse findAll() {
        return new ProductListResponse(productRepository.findAll()
                                                        .stream()
                                                        .map(ProductToResponseConverter::convert)
                                                        .toList());
    }

    public void delete(UUID uuid) {
        Product deletedProduct = find(uuid);
        deletedProduct.setDeleted(true);
        save(deletedProduct);
    }
}

