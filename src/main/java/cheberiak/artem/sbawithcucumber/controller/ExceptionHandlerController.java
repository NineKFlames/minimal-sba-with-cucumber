package cheberiak.artem.sbawithcucumber.controller;

import cheberiak.artem.sbawithcucumber.model.dto.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

@RestControllerAdvice(annotations = RestController.class)
@Slf4j
public class ExceptionHandlerController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(HttpServletRequest request, Throwable ex) {
        log.error(ex.getMessage(), ex);
        HttpStatus status = getStatus(request);
        String message = ex.getMessage();
        ResponseStatus responseStatusAnn = ex.getClass().getAnnotation(ResponseStatus.class);
        if (responseStatusAnn == null && ex.getClass().getSuperclass() != null) {
            responseStatusAnn = ex.getClass().getSuperclass().getAnnotation(ResponseStatus.class);
        }
        if (responseStatusAnn != null) {
            status = responseStatusAnn.value();
        }

        ErrorResponse errorResponse = new ErrorResponse(status.value(), message, ex.getClass().getSimpleName());

        return new ResponseEntity<>(errorResponse, status);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleNotValidException(
            MethodArgumentNotValidException ex, HttpServletRequest request) {
        log.error(ex.getMessage(), ex);
        String message = ex.getBindingResult()
                           .getAllErrors()
                           .stream()
                           .map(DefaultMessageSourceResolvable::getDefaultMessage)
                           .collect(Collectors.joining("\n"));

        ErrorResponse errorResponse = new ErrorResponse(400, message, ex.getClass().getSimpleName());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.valueOf(statusCode);
    }
}