package cheberiak.artem.sbawithcucumber.controller;

import cheberiak.artem.sbawithcucumber.model.dto.ProductCreationRequest;
import cheberiak.artem.sbawithcucumber.model.dto.ProductListResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductResponse;
import cheberiak.artem.sbawithcucumber.model.dto.ProductUpdateRequest;
import cheberiak.artem.sbawithcucumber.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@RestController
@RequestMapping(value = "api/v1/product")
@RequiredArgsConstructor
@Slf4j
public class ProductController {
    private final ProductService productService;

    @ApiOperation(value = "Find a product by uuid.")
    @GetMapping(value = "{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductResponse getProductInfoById(
            @PathVariable @Valid @NotNull @ApiParam("The id of desired product.") UUID uuid) {
        return productService.findById(uuid);
    }

    @ApiOperation(value = "Get a list of all products.")
    @GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductListResponse getProductsList() {
        return productService.findAll();
    }

    @ApiOperation(value = "Update a product by uuid.")
    @PutMapping(value = "{uuid}",
                produces = MediaType.APPLICATION_JSON_VALUE,
                consumes = MediaType.APPLICATION_JSON_VALUE)
    public ProductResponse updateProductInfoById(
            @PathVariable @Valid @NotNull @ApiParam("The id of product to update.") UUID uuid,
            @RequestBody @Valid @ApiParam("Updated product details.") ProductUpdateRequest updateRequest) {
        return productService.updateById(uuid, updateRequest);
    }

    @ApiOperation(value = "Create a new product.")
    @PostMapping(value = "/new",
                 produces = MediaType.APPLICATION_JSON_VALUE,
                 consumes = MediaType.APPLICATION_JSON_VALUE)
    public ProductResponse createProduct(
            @RequestBody @Valid @ApiParam("New product details.") ProductCreationRequest creationRequest) {
        return productService.saveNew(creationRequest);
    }


    @ApiOperation(value = "Delete a product.")
    @DeleteMapping(value = "{uuid}")
    public void createProduct(
            @PathVariable @Valid @NotNull @ApiParam("The id of product to delete.") UUID uuid) {
        productService.delete(uuid);
    }
}
